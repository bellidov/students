package ru.niit.students;

import java.io.Serializable;

class Student implements Serializable
{
	private String firstName, middleName, lastName;
	private String facu;
	private String birth;
	private String group;
	
	public Student(String firstName, String middleName, String lastName, String facu, String birth, String group){
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.facu = facu;
		this.birth = birth;
		this.group = group;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public String getFacu(){
		return facu;
	}
	public String getBirth(){
		return birth;
	}
	public String getGroup(){
		return group;
	}
}
