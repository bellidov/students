package ru.niit.students;

public class ByGroupFilter extends AbstractFilter
{
	private String group;
	public ByGroupFilter(String group){
		this.group = group;
	}
	public boolean isSatisfy(Student st){
		return st.getGroup().equals(group);
	}
}
