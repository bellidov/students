package ru.niit.students;

import java.io.*;

public class MainClass 
{
	public static void main(String[] args) throws FileNotFoundException 
	{		
		InMemoryRepository repo = null;
		if(new File("data.txt").exists()){
			
			ObjectInputStream is = null;
			
			try {
				is = new ObjectInputStream(new FileInputStream(new File("data.txt")));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				repo = (InMemoryRepository)is.readObject();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
		{repo = new InMemoryRepository();}
		
		Menu menu = new Menu(repo);
		menu.start();	
		
		
	}
}
