package ru.niit.students;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class InMemoryRepository implements IRepository, Serializable
{
	private List<Student> list;
	
	public InMemoryRepository()
	{
		list = new LinkedList<Student>();
	}

	@Override
	public void add(Student s) 
	{
		list.add(s);
	}

	@Override
	public int delete(String firstName, String middleName, String lastName, String facu, String group) 
	{
		int count = 0;
		for(int i = 0; i < list.size(); i++)
		{
			if(list.get(i).getFirstName().equals(firstName) && 
			   list.get(i).getMiddleName().equals(middleName) &&
			   list.get(i).getLastName().equals(lastName) &&
			   list.get(i).getFacu().equals(facu) && 
			   list.get(i).getGroup().equals(group)) 
			{
			   list.remove(i);
			   count++;
			}
		}
		return count;
	}
	
	@Override
	public List<Student> find(String firstName, String middleName, String lastName) 
	{
		List<Student> stu = new LinkedList<Student>();
		for(Student s : list)
		{
			if(s.getFirstName().equals(firstName) && 
			   s.getMiddleName().equals(middleName) &&
			   s.getLastName().equals(lastName)) 
			{
				stu.add(s);
			}
		}
		return stu;		
	}
	
	public List<Student> getAll()
	{
		return list;
	}
}

