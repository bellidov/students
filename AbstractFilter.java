package ru.niit.students;

public abstract class AbstractFilter 
{
	abstract public boolean isSatisfy(Student st);
}
