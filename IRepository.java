package ru.niit.students;

import java.io.Serializable;
import java.util.List;

public interface IRepository
{
	void add(Student s);
	int delete(String firstName, String middleName, String lastName, String facu, String group);
	List<Student> find(String firstName, String middleName, String lastName);
	List<Student> getAll();
}