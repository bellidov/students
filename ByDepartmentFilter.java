package ru.niit.students;

public class ByDepartmentFilter extends AbstractFilter
{
	private String department;
	public ByDepartmentFilter(String department){
		this.department = department;
	}
	public boolean isSatisfy(Student st){
		return st.getFacu().equals(department);
	}
}
