package ru.niit.students;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.util.Collections;
import java.util.List;

public class Menu
{
	private IRepository repository;
	
	public Menu(IRepository rep)
	{
		this.repository = rep;
	}
	
	public void start()
	{
		int option = '0';
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		while(true){
			System.out.println("Select a option:\n"
					+ "1 - add new student\n"
					+ "2 - delete student\n"
					+ "3 - find student\n"
					+ "4 - show all students\n"
					+ "5 - save all changes\n"
					+ "0 - exit from the program");
			try{
				option = reader.readLine().charAt(0);
			}
			catch(IOException e){
				e.printStackTrace();
			}
			
			switch(option){
				case '1':
					AddNewStudent();
					break;
				case '2':
					DeleteStudent();
					break;
				case '3':
					FindStudent();
					break;
				case '4':
					ShowAllStudents();
					break;
				case '5': 
					Save();
					break;
				case '0':
					Exit();
			}
		}
	}
	
	private void Save(){
		
		try {
			ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(new File("data.txt")));
			os.writeObject(this.repository);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	private void AddNewStudent()
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String firstName = null, middleName = null, lastName = null, facu = null, birth = null;
		String group = null;
		try{
			System.out.println("Please, enter Last Name: ");
			lastName = reader.readLine();
			
			System.out.println("Please, enter First Name: ");
			firstName = reader.readLine();
			
			System.out.println("Please, enter Patronymic: ");
			middleName = reader.readLine();
			
			System.out.println("Please, enter facu: ");
			facu = reader.readLine();
			
			System.out.println("Please, enter birth: ");
			birth = reader.readLine();
			
			System.out.println("Please, enter group: ");
			group = reader.readLine();
			
			repository.add(new Student(firstName, middleName, lastName, facu, birth, group));
			System.out.println("\nStudent successfully added!\n");
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	private void DeleteStudent()
	{
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Please, enter lastName: ");
			String lastName = reader.readLine();
			System.out.println("Please, enter firstName: ");
			String firstName = reader.readLine();
			System.out.println("Please, enter patronymic: ");
			String middleName = reader.readLine();
			System.out.println("Please, enter facu: ");
			String facu = reader.readLine();
			System.out.println("Please, enter group: ");
			String group = reader.readLine();
			
			int count = repository.delete(firstName, middleName, lastName, facu, group);
			if(count != 0){
				System.out.println("\n" + count + " student" + (count > 1 ? "s " : " ") +"successfully deleted!\n");
			}
			else{
				System.out.println("\nStudent was not found!\n");
			}
			
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	private void FindStudent(){
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Please, enter lastName: ");
			String lastName = reader.readLine();
			System.out.println("Please, enter firstName: ");
			String firstName = reader.readLine();
			System.out.println("Please, enter patronymic: ");
			String middleName = reader.readLine();
			
			List<Student> result = repository.find(firstName, middleName, lastName);
			
			System.out.println("\nFIO \t\t\tDepartment \tGroup");
			if(result.size() > 0){
				for(Student s : result){
					System.out.println(s.getLastName() + " "+ s.getFirstName() + " " + s.getMiddleName() + "\t" + 
									   s.getFacu() + "\t"+
									   s.getGroup());	
				}
				System.out.println("Number of Students with this name: " + result.size() + "\n");
			}
			else
			{
				System.out.println("Students with this name was not found.\n");
			}
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	private void ShowAllStudents()
	{
		// podmenu
		System.out.println("Select a option: \n"
				+ "1 - Show students by Department");
		System.out.println("2 - Show students by Group");
		System.out.println("3 - Show all students");
		
		String option = "0";
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		try{
			while(true) {
				option = reader.readLine();
				
				if(!option.equals("1") && !option.equals("2") && !option.equals("3")){
					System.out.println("Please, enter 1, 2 or 3");
				}
				else{
					break;
				}
			}
		}
		catch(IOException e){
			e.printStackTrace();
		}
	

		AbstractFilter filter;

		if(option.equals("1")){
			String depa = null;
			try{			
				System.out.print("Enter the department: \n");
				depa = reader.readLine();
			}
			catch(IOException e){
				e.printStackTrace();
			}
			filter = new ByDepartmentFilter(depa);
		}
		else if(option.equals("2")){
			String group = null;
			try{			
				System.out.print("Enter the group: \n");
				group = reader.readLine();
			}
			catch(IOException e){
				e.printStackTrace();
			}
			filter = new ByGroupFilter(group);
		}
		else {
			filter = null;
		}
		
		Collections.sort(repository.getAll(), new StudentsComparer());
		System.out.println("\nFIO \t\t\t\t\tDepartment \t\tGroup");
		for(Student s : repository.getAll())
		{
			if(filter == null || filter.isSatisfy(s))
			{
				System.out.println(s.getLastName() + " "+ s.getFirstName() + " " + s.getMiddleName() + "\t\t\t" + 
								   s.getFacu() + "\t\t\t"+
								   s.getGroup());	
			}
		}
		
		System.out.println();
	}
	
	private void Exit()
	{
		System.exit(1);
	}
}