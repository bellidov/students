package ru.niit.students;

import java.util.Comparator;

public class StudentsComparer implements Comparator<Student>
{
	@Override
	public int compare(Student st1, Student st2) {
		String name1 = st1.getLastName();
		String name2 = st2.getLastName();
		int comp = name1.compareTo(name2);
		
		if(comp > 0)
			return 1;
		else if(comp < 0)
			return -1;
		else {
			name1 = st1.getFirstName();
			name2 = st2.getFirstName();
			comp = name1.compareTo(name2);
			if(comp > 0)
				return 1;
			else if(comp < 0)
				return -1;
			else {
				name1 = st1.getMiddleName();
				name2 = st2.getMiddleName();
				comp = name1.compareTo(name2);
				if(comp > 0)
					return 1;
				else if(comp < 0)
					return -1;
				else {
					return 0;
				}
			}
		}
	}
}
